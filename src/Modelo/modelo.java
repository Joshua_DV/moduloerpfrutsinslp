// MODULO DE REPORTES (Modelo)

package Modelo;
import Conexion.conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class modelo {
    public String nom = "";
    public conexion con = new conexion();

    public boolean pruebaDatos(){
        boolean res;
        try{
            //abrir la bd
            Connection connect = con.abrirConexion();
            Statement st =  connect.createStatement();
            //ejecutar consulta
            ResultSet rs = st.executeQuery("select CONCAT(nombres, ' ',apellidoPaterno,' ', apellidoMaterno) as \"Nombre completo\" from dimempleados where idEmpleados = 1;");
            DefaultTableModel dtm = new DefaultTableModel();
            ResultSetMetaData rsMD = rs.getMetaData();
            int columnas = rsMD.getColumnCount();
            for(int i=1; i <= columnas; i ++){
                dtm.addColumn(rsMD.getColumnLabel(i));
            }
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i<columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            if(dtm.getRowCount() != 0){
                nom = String.valueOf(dtm.getValueAt(0, 0));
                res = true;
            }
            else{
                res = false;
            }
            con.cerrarConexion(connect);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error en consulta.");
            res = false;
        }
        return res;
    }

    public DefaultTableModel RendimientoPerdidasPA1(){
        try{
            Connection connect = con.abrirConexion();
            Statement s = connect.createStatement();
            ResultSet rs = s.executeQuery("Call ConsultaRendimientoPerdidas();");  // Ejecuta la consulta
            DefaultTableModel dtm = new DefaultTableModel();    
            
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); // regresa el número de columnas
            // ciclo para las columnas
            for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            
            // ciclo para las filas
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error en la consulta 1.");
            return null;
        }
    }

    public DefaultTableModel ClienteDemandaExportacionPA2(){
        try{
            Connection connect = con.abrirConexion();
            Statement s = connect.createStatement();
            ResultSet rs = s.executeQuery("Call ConsultaClienteDemandaExportacion();");  // Ejecuta la consulta
            DefaultTableModel dtm = new DefaultTableModel();    
            
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); // regresa el número de columnas
            // ciclo para las columnas
            for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            
            // ciclo para las filas
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error en la consulta 1.");
            return null;
        }
    }

    public DefaultTableModel TendenciaProduccionMangoPA3(){
        try{
            Connection connect = con.abrirConexion();
            Statement s = connect.createStatement();
            ResultSet rs = s.executeQuery("Call ConsultaTendenciaProduccionMango();");  // Ejecuta la consulta
            DefaultTableModel dtm = new DefaultTableModel();    
            
            ResultSetMetaData rsMd =  rs.getMetaData();
            int columnas = rsMd.getColumnCount(); // regresa el número de columnas
            // ciclo para las columnas
            for(int i=1; i <= columnas; i++){  // sirve para obtener los nombres de cada columna (encabezado)
                dtm.addColumn(rsMd.getColumnLabel(i));
            }
            
            // ciclo para las filas
            while(rs.next()){
                Object[] fila = new Object[columnas];
                for(int i=0; i< columnas; i++){
                    fila[i] = rs.getObject(i+1);
                }
                dtm.addRow(fila);
            }
            return dtm;
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error en la consulta 1.");
            return null;
        }
    }
    
}
