package Controlador;

import Modelo.modelo;
import Vista.Vista;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryToPieDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.TableOrder;

public class controlador implements ActionListener, MouseListener{
    private modelo mod = new modelo();
    private Vista vista = new Vista();
    
    public controlador(modelo mod, Vista vista){
        this.mod = mod;
        this.vista = vista;
        this.vista.cbPregunta.addActionListener(this);
    }

    public void iniciarVentana(){
        vista.setTitle("::Reportes de FrutsinSLP::");
        //vista.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Vista/Imagenes/logoM.png")));
        vista.pack();
        vista.setResizable(false);
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        vista.cbPregunta.setEnabled(true);
        vista.tblRespuesta.setVisible(true);
        vista.tblRespuesta.setCellSelectionEnabled(false);
        vista.cbPregunta.addItem("Seleccione una pregunta");
        vista.cbPregunta.addItem("Rendimiento en cuanto a perdidas");
        vista.cbPregunta.addItem("Clientes y exportación");
        vista.cbPregunta.addItem("Tendencia de producción");
    }
    
    public void accion(int pregunta){
        switch (pregunta) {
            case 0:
                System.out.println("No selecciono nada");
                DefaultTableModel newModel = new DefaultTableModel();
                vista.tblRespuesta.setModel(newModel);
                break;
            case 1:
                System.out.println("P1");
                vista.tblRespuesta.setModel(mod.RendimientoPerdidasPA1());
                JFreeChart barChart = ChartFactory.createBarChart(
                    "Rendimiento en Pérdidas de Mangos", 
                    "Situaciones", 
                    "Cantidades",
                    extraerDatos(1,vista.tblRespuesta.getModel()),
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false
                );
                graficarDatos(barChart);
                break;
            case 2:
                System.out.println("P2");
                vista.tblRespuesta.setModel(mod.ClienteDemandaExportacionPA2());
                JFreeChart piechart = ChartFactory.createPieChart(
                    "Participación de clientes en exportaciones",
                    extraerPie(vista.tblRespuesta.getModel()),
                    true,
                    true,
                    false
                );
                graficarPie(piechart);
                break;
            case 3:
                System.out.println("P3");
                vista.tblRespuesta.setModel(mod.TendenciaProduccionMangoPA3());
                JFreeChart lineChart = ChartFactory.createLineChart(
                    "Tendencia de Producción de Mangos",
                    "Año",
                    "Cantidades",
                    extraerDatos(3,vista.tblRespuesta.getModel()),
                    PlotOrientation.VERTICAL,
                    true,
                    true,
                    false
                );
                graficarDatos(lineChart);
                break;
            default:
                break;
        }
    }
    
    public void graficarPie(JFreeChart chart){
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setMaximumDrawHeight(vista.pnlGraf.getHeight()+50);
        JScrollPane scroll = new JScrollPane(chartPanel);
        vista.pnlGraf.setLayout(new BorderLayout());
        vista.pnlGraf.add(scroll, BorderLayout.NORTH);
    }
    
    public void graficarDatos(JFreeChart chart){
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setMaximumDrawHeight(vista.pnlGraf.getHeight()-150);
        JScrollPane scroll = new JScrollPane(chartPanel);
        vista.pnlGraf.setLayout(new BorderLayout());
        vista.pnlGraf.add(scroll, BorderLayout.NORTH);
    }
    
    public PieDataset extraerPie(TableModel dtm){
        DefaultPieDataset datapie = new DefaultPieDataset();
        int rowCount = dtm.getRowCount();
        
        for(int row = 0; row < rowCount; row ++){
                    String nombreCli = (String) dtm.getValueAt(row, 0);
                    Double tonDem = (Double) dtm.getValueAt(row, 1);
                    datapie.setValue(nombreCli, tonDem);
        }
        return datapie;
    }
    
    public DefaultCategoryDataset extraerDatos(int opc, TableModel dtm){
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        int rowCount = dtm.getRowCount();
        switch(opc){
            case 1:
                for(int row = 0; row < rowCount; row ++){
                    String tipoMango = (String) dtm.getValueAt(row, 0);
                    Double tonVen = (Double) dtm.getValueAt(row, 1);
                    Double tonEnt = (Double) dtm.getValueAt(row, 2);
                    Double tonPer = (Double) dtm.getValueAt(row, 3);
                    dataset.addValue(tonVen, tipoMango, "Venta");
                    dataset.addValue(tonEnt, tipoMango, "Entregado");
                    dataset.addValue(tonPer, tipoMango, "Perdida");
                }
                break;
            case 3:
                for (int row = 0; row < rowCount; row++) {
                    Double toneladas = (Double) dtm.getValueAt(row, 0);
                    String tipoMango = (String) dtm.getValueAt(row, 1);
                    int año = (Integer) dtm.getValueAt(row, 2);
                    dataset.addValue(toneladas, tipoMango, String.valueOf(año));
                }
                break;
            default:
                break;
        }        
        return dataset;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(vista.cbPregunta == e.getSource()){
            int pregunta = vista.cbPregunta.getSelectedIndex();
            vista.pnlGraf.removeAll();
            vista.pnlGraf.revalidate();
            vista.pnlGraf.repaint();
            accion(pregunta);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mousePressed(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void mouseExited(MouseEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
